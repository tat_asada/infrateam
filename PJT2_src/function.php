<?php
function get_post($key) {

	$result = "";
	if (isset($_POST[$key])) {
		$result = htmlspecialchars($_POST[$key]);
	}

	return $result;
}
?>