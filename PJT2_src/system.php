<?php
require_once './main_index.php';
require_once './main_down.php';
?>
<div>
	<ol class="topic-path">
	<li class="first"><a href="./index.php">ホーム</a></li>
	<li>サービス・施設</li>
	</ol>
</div>
<div class="page_header">
	<img src="./images/header_system.jpg"><h2>サービス・施設</h2>
</div>
<div class="jump">
	<ul>
	<li><a href="#onsen"><img src="./images/onsen.jpg"><br>天然温泉</a></li>
	<li><a href="#kannai"><img src="./images/kannai.jpg"><br>館内施設</a></li>
	<li><a href="#kihon"><img src="./images/kihon.jpg"><br>基本情報</a></li>
	</ul>
</div>
<div align="center" id=system>
<h2 id="onsen">天然温泉</h2><hr>
	<table>
	<tbody>
		<tr>
		<td width="15" valign="top"></td>
		<td width="650" valign="top"><img src="images/bath.jpg" id="bath1"><br>
		大浴場からは遮るもののない絶景が広がります</td>
		<td width="15" valign="top"></td>
		<td width="220" valign="top"><img src="images/bath2.jpg" id="bath2"><br>
		露天風呂
		<img src="images/bath3.jpg" id="bath2"><br>
		室内風呂</td>
		</tr>
	</tbody>
	</table>
	<table>
	<tbody>
		<tr>
		<td width="15" valign="top"></td>
		<td width="435"><img src="images/bath4.jpg" id="bath3"><br>
		快適なパウダールーム</td>
		<td width="20" valign="top"></td>
		<td><img src="images/bath5.jpg" id="bath3"><br>
		テラス付きのサロンがある脱衣スペース</td>
		</tr>
	</tbody>
	</table>
</div>
<div align="center" id=system2>
<h2 id="kannai">館内風景</h2><hr><br>
	<table>
	<tbody>
		<tr>
		<td width="600" >
		<img src="images/system.jpg" id="view1">
		</td>
		<td width="235" valign="top">
		<img src="images/system2.jpg" id="view2"><br><br>
		<img src="images/system3.jpg" id="view2"><br>
		</td>
		</tr>
	</tbody>
	</table><br>
	<table>
	<tbody>
		<tr>
		<td width="424"><img src="images/system4.jpg" id="view3"><br>
		</td>
		<td width="20" valign="top"></td>
		<td width="424"><img src="images/system5.jpg" id="view3"><br>
		</td>
		</tr>
	</tbody>
	</table>
<br><h2 id="kihon">基本情報</h2><hr><br>
<div align="center" id=system3>
	<table border="1">
		<tr>
			<th>総部屋数</th>
			<td>20室</td>
		</tr>
		<tr>
			<th>インターネット</th>
			<td>フロントロビーにて無線LANによりインターネットが利用できます。端末の貸し出しは行っていません。</td>
		</tr>
		<tr>
			<th>アメニティ</th>
			<td>ハンドタオル、シャンプー、リンス、ボディソープ、ドライヤー、歯ブラシ<br>
			石けん、羽毛布団、くし・ブラシ、バスタオル、浴衣、髭剃り
			</td>
		</tr>
		<tr>
			<th>駐車場</th>
			<td>無料駐車場30台 (先着順)</td>
		</tr>
		<tr>
			<th>サービス</th>
			<td>送迎バス、宅配便、モーニングコール、おしゃれ浴衣貸出（レンタル料500円/税別）</td>
		</tr>
		<tr>
			<th>ご利用可能<br>クレジットカード</th>
			<td>VISA、JCB、American Express、Diner's Club、UC、NICOS、Bank Card、UFJ Card、
Master Card、Saison、ORICO、デビットカード
			</td>
		</tr>
		<tr>
			<th>バリアフリー対応</th>
			<td>車椅子可、貸出用車椅子、バリアフリー用トイレ、客室内に洋式トイレあり、客室内トイレに手すりあり、大浴場浴槽に手すりあり、館内に車椅子利用可能なトイレあり、高齢者用料理への対応可能、アレルギーに配慮した料理への対応可能
※ご不便なく安心してご利用いただくために、ご予約いただく際にご確認ください。 </td>
		</tr>
	</table><br>
</div>