<?php

//外部関数とデータベースへの接続する外部ファイルを読み込み
require("db_connect.php");
require("function.php");

//htmlのテンプレを読み込み
require_once ("./main_index.php");
require_once ("./main_down.php");

//ここまで接続など読み込み
//-------------------------------------------------------------------------
//ここからpostデータの処理


//POSTデータの有無を確認後、htmlspecialcharsに変換
//無い場合は""に
$name = get_post('name');
$phone_number = get_post('phone_number');
$mail_address = get_post('mail_address');
$reserve_date = get_post('reserve_date');

//データベースからmaxcount(最大部屋数)を取ってくる
$stmt = $dbh->query("select * from information where id = 1");
foreach ($stmt as $row) {
	$maxcount=$row['maxcount'];
}

//ここからhtmlとデータ入力
echo "<!DOCTYPE html>";

print <<< DOC
<html>
	<head>
	<link rel="stylesheet" style type="text/css" href="css/common.css">
	<br>
	<title>旅の宿 江波戸</title>
	</head>
	<body>
		<div>
		<ol class="topic-path">
			<li class="first"><a href="./index.php">ホーム</a></li>
DOC;
      //plan.phpかdetil.phpから来たならパンくずリスト追加
			if(!empty($_GET['name'])){
				$p=$_GET['name'];
			echo"<li><a href='./plan.php'>プラン一覧</a></li>";
			echo"<li><a href='./detail.php?name=$p'>プラン詳細</a></li>";
		}

print <<< DOC
			<li>予約フォーム</li>
		</ol>
	</div>
	<div align="center" id=form>
		<h1>予約フォーム</h1>
			<font size='2' color='red'>*必須項目はご入力ください</font><br><br><br>
			<table border ='1' id='formtable'>

<form method = "post" action="./accept.php">
			<p>
				<tr><th><font size='1' color='red'>*</font>名前</th><td><input type="text" placeholder="お名前" name='name' value="{$name}" ></td></tr>
				<tr><th><font size='1' color='red'>*</font>TEL</th><td><input type="text" placeholder="電話番号" name='phone_number' value="{$phone_number}">
				<br>
					<font size='1' color='red'>-(ﾊｲﾌﾝ)なしでご入力ください。</font></td></tr>
				<tr><th><font size='1' color='red'>*</font>メールアドレス</th><td><input type="text" placeholder="メールアドレス" name='mail_address' value="{$mail_address}"></td></tr>
DOC;

			//人数
			echo "<tr>";
					echo "<th><font size='1' color='red'>*</font>人数</th>"."<td><select name = 'persons'>\n";
						for ($person = 1 ; $person <= $maxcount * 4 ; $person++) {
							echo "<option value = '$person'>" . $person . "</option>\n";
						}
					echo "</select>"."名様"."</td><br><br>";
			echo "</tr>";

			//予約部屋数
			echo "<tr>";
			echo "<th><font size='1' color='red'>*</font>予約部屋数</th>"."<td><select name = 'room'>\n";
				for ($room = 1 ; $room <= $maxcount ; $room++) {
					echo "<option value = '$room'>" . $room . "</option>\n";
				}
			echo "</select>\n"."室".
			"<br><font size='1' color='red'>1部屋4名様までとなっております。<br>
			3名様以下のお部屋を複数ご予約の場合はお電話でご相談ください。</font></td><br>";
			echo "</tr>";

			//プラン選択
			echo "<tr>";
				echo "<th height='100px'><font size='1' color='red'>*</font>プラン選択</th>"."<td height='100px'>";
					$stmt = $dbh->query('select * from plan where delete_flg=0');
					foreach ($stmt as $row) {
						echo "<input type='radio' required name='plan_id' value=".$row['id']. ">".$row['plan_name'];
						echo "<br>";
					}
				echo "</td>";
			echo "</tr>";
			echo "<br>";

		print <<< DOC
			<tr>
				<th><font size='1' color='red'>*</font>予約日</th>
				<td><input type="date" name='reserve_date' value="{$reserve_date}"></td><br><br>
			</tr>

			</table>
				<div id='kiyaku'><input type="checkbox" name="agree" value="check" required >同意する
					<input name="openWin" type="button" value="個人情報規約" onClick="window.open
						('./kiyaku.html','','scrollbars=yes,width=500,height=300,');"></div>
					<a class="btn" href="accept1.php"><input type="submit" value="登録" id= "btn"></a>
</form><br>

		</div>
		<footer></footer>
	</body>
</html>
DOC;
