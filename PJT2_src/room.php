<?php
require_once './main_index.php';
require_once './main_down.php';
?>
<div>
	<ol class="topic-path">
		<li class="first"><a href="./index.php">ホーム</a></li>
		<li>お部屋</li>
	</ol>
</div>
<div class="page_header">
	<img src="./images/header.jpg">
	<h2>お部屋</h2>
</div>
<div class="jump">
	<ul>
	<li><a href="#tsubaki"><img src="./images/room1.jpg"><br>椿の間</a></li>
	<li><a href="#botan"><img src="./images/room2.jpg"><br>牡丹の間</a></li>
	<li><a href="#sakura"><img src="./images/room3.jpg"><br>桜の間</a></li>
	</ul>
</div>
<div class=room>
	<table id="tsubaki">
		<th><h3 class="addbullet">椿の間</h3></th>
		<tr>
			<td width="600"><img src="images/room1.jpg" class="room1"></td>
			<td width="235" valign="top"><img src="images/room1_2.jpg"
				class="room2"><br> <br> <img src="images/room1_3.jpg" class="room2"><br></td>
		</tr>
	</table>
	<table>
		<tr width="450">
			<td vertical-align="top">
				<div class=roomsyoukai>
					<font size=2> 竹・和紙をテーマとしたお部屋です<br> 専用面積 40.79㎡（禁煙）<br>
					<br>
					<br> ・専用露天風呂<br> ・3D液晶テレビ（60インチ）+ DVD<br> ・全自動エスプレッソマシン<br>
						※有機栽培のコーヒー豆をご用意しております<br> ・加湿機能付き空気清浄機
					</font>
				</div>
			</td>

			<td valign="top"><img src="images/room_madori1.png" class="madori"></td>
		</tr>
	</table>

	<table id="botan">
		<th><h3 class="addbullet">牡丹の間</h3></th>
		<tr>
			<td width="600"><img src="images/room2.jpg" class="room1"></td>
			<td width="235" valign="top"><img src="images/room2_2.jpg"
				class="room2"><br> <br> <img src="images/room2_3.jpg" class="room2"><br></td>
		</tr>
	</table>
	<br>
	<table>
		<tr width="500">
			<td vertical-align="top">
				<div class=roomsyoukai>
					<font size=2> 土をテーマとしたお部屋です<br> 専用面積 43.26㎡（禁煙）<br>
					<br>
					<br> ・専用露天風呂<br> ・3D液晶テレビ（60インチ）+ DVD<br> ・全自動エスプレッソマシン<br>
						※有機栽培のコーヒー豆をご用意しております<br> ・加湿機能付き空気清浄機
					</font>
				</div>
			</td>
			<td valign="top"><img src="images/room_madori1.png" class="madori"></td>
		</tr>
	</table>

	<table id="sakura">
		<th><h3 class="addbullet">桜の間</h3></th>
		<tr>
			<td width="600"><img src="images/room3.jpg" class="room1"></td>
			<td width="235" valign="top"><img src="images/room3_2.jpg"
				class="room2"><br> <br> <img src="images/room3_3.jpg" class="room2"><br></td>
		</tr>
	</table>
	<br>
	<table>
		<tr width="500">
			<td vertical-align="top">
				<div class=roomsyoukai>
					<font size=2> 石をテーマとしたお部屋です<br> 専用面積 68.33㎡（禁煙）<br>
					<br>
					<br> ・専用露天風呂<br> ・3D液晶テレビ（60インチ）+ DVD<br> ・全自動エスプレッソマシン<br>
						※有機栽培のコーヒー豆をご用意しております<br> ・加湿機能付き空気清浄機
					</font>
				</div>
			</td>
			<td valign="top"><img src="images/room_madori3.png" class="madori"></td>
		</tr>
	</table>
</div>
</div>