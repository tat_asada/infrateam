<?php
require_once ("./main_index.php");
?>
<div>
	<ol class="topic-path">
		<li class="first"><a href="./index.php">ホーム</a></li>
		<li>アクセス</li>
	</ol>
</div>
<div class="page_header">
	<img src="./images/access.JPG">
	<h2>アクセス</h2>
</div>
<!-- page_header -->

<div id="main">
	<p align="center"><< 交通のご案内 >></p>
	<hr align="center" width="830px">
	<br>
	<div id="access_guide">
		<img id="access_png" src="./images/access_root.png">
	</div>
	<!-- access_guide -->
	<br> <br>
	<table>
		<tr>
			<td>
				<div id="access_route1">
					<p2>●新幹線こだま</p2>
					<li>東京ー熱海（55分）</li>
					<li>名古屋ー熱海（1時間50分）</li>
					<li>新大阪ー熱海（3時間15分）</li>
					<li>岡山ー熱海（4時間15分）</li>
					<li>※熱海ー湯河原（東海道線7分・車約10分）</li> <br>
					<p2>●東海道線（東京－湯河原）</p2>
					<li>踊り子号急行（1時間10分）</li>
					<li>快速アクティー（1時間25分）</li>
					<li>普通（1時間45分）</li>
				</div>
			</td>
			<td width="15px"></td>
			<td>
				<div id=googlemap>
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3261.6399675482494!2d139.05749221506858!3d35.1655987803177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6019bd40cfc6f031%3A0x3138d3faee149a0b!2z5peF44Gu5a6_IOS4gOiRiQ!5e0!3m2!1sja!2sjp!4v1465785596372"
						width="300" height="300" frameborder="0" style="border: 0"
						allowfullscreen></iframe>
				</div> <!-- googlemap -->
			</td>
		</tr>
		<br>
		<tr>
			<td>
				<div id="access_route2">
					<p2>●小田急線</p2>
					<li>新宿ー小田原（ロマンスカー・1時間）ー小田原ー湯河原（JR東海道線25分） ※湯河原ー「江波戸」</li>
					<li>車15分・バス20分（「奥湯河原」で下車、徒歩10分）</li> <br>
					<p2>●東名高速道路</p2>
					<li>・東京－湯河原 「江波戸」（厚木IC－小田原・小田原西IC）</li>
					<li>約2時間30分 ・名古屋ー湯河原 「江波戸」（御殿場IC) 約6時間</li>
					</ul>
				</div>
			</td>
		</tr>
	</table>
	<!-- access_route -->

</div>
<!-- /#main -->

<?php
require_once ("./main_down.php");
