<!DOCTYPE html>
<html>
<head>
<title>管理画面|ログイン</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
	<header align="center">
		<h1>
			<a href='./admin_index.php'>管理画面</a>
		</h1>
	</header>
	<div id="main">
		<div id="contents">

			<div id='login_form'>
				<h1>ログイン</h1>
				<br>
				<form method='post'>
					ユーザー名 <input type='text' name='user'> <br>
					<br> パスワード <input type='password' name='pass'> <br>
					<br> <input type='submit' name='login' value='ログイン' id='login'>
				</form>
			</div>
		</div>

<?php
session_start ();
// 外部関数の呼び出し
require_once ("../function.php");
// $_SESSIONを空にしておく
$_SESSION = array ();

// フォームに入力されたユーザー名とパスワードを$userと$passに格納
// ログインボタンのvalueを$loginに格納
$user = get_post ( 'user' );
$pass = get_post ( 'pass' );
$login = get_post ( 'login' );

// $loginがログインだったら下の処理に入り、ユーザー名とパスワードが一致していたらセッションでログインフラグを付与
// 一致しなかったらエラーメッセージを表示
if ($login == "ログイン") {
	if ($user == 'administor' and $pass == 'ebato') {
		$_SESSION ['login_flg'] = "login_now";
	} else {
		echo "<br><font color='red'>ユーザー名とパスワードが一致しません</font>";
	}
}
// ログインフラグがあれば管理画面のトップページへリダイレクト
if (isset ( $_SESSION ['login_flg'] )) {
	header ( "Location: ./admin_index.php" );
	exit ();
}
?>
<footer align="center">
			<h1 id='ebato_inn'>Ebato.Inn</h1>
		</footer>
