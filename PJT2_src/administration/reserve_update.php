<?php
session_start();
// セッションがなかったらログインページにリダイレクト
if (!isset($_SESSION['login_flg'])) {
	header( "Location: login.php" ) ;
}
// 外部関数とDB接続を呼び出し
require_once '../function.php';
require ("../db_connect.php");
// 初期値の変数
$id = $_GET['id'];
$stmt = $dbh->query("SELECT * FROM information where id = '$id'");
$result = $stmt->fetch(PDO::FETCH_ASSOC);

$date=$result['reserve_date'];
$persons=$result['persons'];
$room=$result['room'];
$plan_id=$result['plan_id'];
$delete=$result['delete_flg'];

if($delete == 0){
	$display="表示";
}
if($delete ==1){
	$display="非表示";
}

$stmt = $dbh->query("select plan_name from plan where id = '$plan_id'");
$result = $stmt->fetch(PDO::FETCH_ASSOC);

$plan=$result['plan_name'];
// ここまで

$change=get_post('change');
$update_date=get_post('date');
$update_persons=get_post('persons');
$update_room=get_post('room');
$update_plan=get_post('plan');
$update_express = get_post('express');

require '../db_connect.php';
if ($change == '変更') {
	if (! empty ( $update_date )) {
		$sql = $dbh->query("update information set reserve_date='$update_date' where id='$id'");
		$stmt->execute ($sql);
	}
	if (! empty ( $update_persons )) {
		$sql = $dbh->query("update indormation set persons='$update_persons' where id='$id'");
		$stmt->execute($sql);
	}
	if (! empty ( $update_room )) {
		$sql = $dbh->query("update information set room='$update_room' where id='$id'");
		$stmt->execute($sql);
	}
	if (! empty ( $update_plan )) {
		$sql = $dbh->query("update information set plan_id='$update_plan' where id='$id'");
	}
	if ( $update_express !== "") {
		$sql = $dbh->query("update information set delete_flg='$update_express' where id='$id'");
		$stmt->execute($sql);
	}
	header( "Location: ./reserve_update.php?id=$id" );
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
<title>管理画面|予約情報・変更</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
<header align="center"><h1>管理画面</h1></header>
<div id="main">
<div id="side">
<ul>
<li>
<button id='top'><a href='./admin_index.php'>トップ</a></button>
</li>
<li>
<button id='plan'><a href="./plan_info.php">プラン情報</a></button>
</li>
<li>
<button id='reserve'><a href="./reserve.php">予約情報</a></button>
</li>
<li>
<button id = 'img'><a href='./image_update.php'>画像変更</a></button>
</li>
<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
<li>
<button id='logout'><a href=logout.php>ログアウト</a></button>
</li>
</ul>
</div>
<div id="contents">
<h1>予約情報・変更</h1>
<br>
<form method='post' id='form'>
<table align= 'center'  id='plan_info' >

<tr>
<th>予約日（変更前）：<font color='#DC143C'></th></font><td><?php echo $date ?></td>
</tr>

<tr>
<th>予約日（変更後）：<font color='#DC143C'></th></font><td><input type = 'date' name='date'></td>
</tr>

<tr>
<th>人数（変更前）：<font color='#DC143C'></th></font><td><?php echo $persons ?></td>
</tr>

<tr>
<th>人数（変更後）：<font color='#DC143C'></th></font><td><input type='number' name='persons' min=1></td>
</tr>

<tr>
<th>部屋数（変更前）：<font color='#DC143C'></th></font><td><?php echo $room ?></td>
</tr>

<tr>
<th>部屋数（変更後）：<font color='#DC143C'></th></font><td><input type='number' name='room'></td>
</tr>

<tr>
<th>プラン（変更前）：<font color='#DC143C'></th><td></font><?php echo $plan ?></td>
</tr>

<tr>
<th>プラン（変更後）：<font color='#DC143C'></th></font>
<td>
<?php
// デリートフラグが0のプラン名とidをセレクトして回してラジオボタンと共に表示
$stmt = $dbh->query("select plan_name, id from plan where delete_flg=0");
foreach ($stmt as $row){
	$plan_name=$row['plan_name'];
	$plan_number=$row['id'];
	echo "<input type='radio' name='plan' value='$plan_number'>$plan_name"."<br>";
}
?>
</td>
</tr>

<tr>
<th>現在の表示設定：<font color='#DC143C'></th></font><td><?php echo $display ?></td>
</tr>

<tr>
<th>表示の変更</th>
<td><input type='radio' name='express' value=0>表示する
<input type='radio' name='express' value=1>表示しない</td>
</tr>

<tr>
<th>予約情報変更の確定</th><td><input type='submit' name='change' value='変更'></td>
</tr>
</table></form>
</div>
<footer align="center"><h1 id='ebato_inn'>Ebato.Inn</h1></footer>
