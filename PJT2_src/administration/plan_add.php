<?php
session_start();

if (!isset($_SESSION['login_flg'])) {
	header( "Location: login.php" ) ;
}

require_once '../db_connect.php';
require_once ("../function.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|メニュー</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
<header align="center"><h1>管理画面</h1></header>
<div id="main">
<div id="side">
<ul>
<li>
<button id='top'><a href='./admin_index.php'>トップ</a></button>
</li>
<li>
<button id='plan'><a href="./plan_info.php">プラン情報</a></button>
</li>
<li>
<button id='reserve'><a href="./reserve.php">予約情報</a></button>
</li>
<li>
<button id='img'><a href='./image_update.php'>画像変更</a></button>
</li>
<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
<li>
<button id='logout'><a href=./logout.php>ログアウト</a></button>
</li>
</ul>
</div>
<div id="contents">

<h1>プラン追加</h1>
<br>
<form method="post" enctype='multipart/form-data'>
<table align= 'center'  id='plan_add' >

<tr>
<th>プラン名<font color='#DC143C'></th></font><td><input type = 'text' name='plan' placeholder='新しいプラン名'></td>
</tr>

<tr>
<th>プラン概要<font color='#DC143C'></th></font><td><textarea rows="10" cols="40" name='detail' placeholder='次の行にいくときは改行してください'></textarea></td>
</tr>

<tr>
<th>プラン金額<font color='#DC143C'></th></font><td><input type='number'  name='price'></td>
</tr>

<tr>
<th>プラン画像<font color='#DC143C'></th></font><td><input type='file' name='image'></td>
</tr>
</table>
<input type='submit' name='change' value='適用' button id='plan_change'>
</form>

<?php
$plan=get_post('plan');
$detail=get_post('detail');
$price=get_post('price');
$submit=get_post('change');

if ($submit == '適用') {
	if(!empty($plan) and !empty($detail) and !empty($price) and !empty($submit)){
		$stmt = $dbh->prepare ( "INSERT INTO plan(plan_name, detail, plan_price) VALUES(?,?,?) " );
		$stmt->execute ( array (
				$plan,
				$detail,
				$price
		) );

		$id = $dbh->lastInsertId ();
		if ($_FILES) {
			$path = pathinfo ( $_FILES ['image'] ['name'] );
			$kakucho = $path ['extension'];

			$file_dir = "../images/";
			$uploadfile = "$file_dir" . "plan" . $id . "." . $kakucho;
			move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $uploadfile );
		}

	}else{
		echo "<font color='red'>プラン情報をすべて入力してください</font>";
	}
}
?>
</div></div>
<footer align="center"><h1 id='ebato_inn'>Ebato.Inn</h1></footer>
</body>
</html>
