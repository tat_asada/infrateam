<?php
session_start ();
// セッションがなかったらログインページにリダイレクト
if (! isset ( $_SESSION ['login_flg'] )) {
	header ( "Location: login.php" );
}
// DBに接続
require ("../db_connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|プラン画像・変更</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
	<header align="center">
		<h1>管理画面</h1>
	</header>
	<div id="main">
		<div id="side">
			<ul>
				<li>
					<button id='top'>
						<a href='./admin_index.php'>トップ</a>
					</button>
				</li>
				<li>
					<button id='plan'>
						<a href='./plan_info.php'>プラン情報</a>
					</button>
				</li>
				<li>
					<button id='reserve'>
						<a href='./reserve.php'>予約情報</a>
					</button>
				</li>
				<li>
					<button id='img'>
						<a href='./image_update.php'>画像変更</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href='./logout.php'>ログアウト</a>
					</button>
				</li>
			</ul>
		</div>
		<div id="contents">
			<h1>プラン画像変更</h1>
			<br>
			<p>
				<font color='green'>画像を変更したいプランを選択してください</font>
			</p>
			<br>
<?php
// planテーブルから、デリートフラグが0のidとプランネームをセレクトして回して取得
// idが一致する画像とプラン名をエコーで表示
$stmt = $dbh->query ( "select id, plan_name from plan where delete_flg=0" );
foreach ( $stmt as $row ) {
	$id = $row ['id'];
	$plan = $row ['plan_name'];

	echo "<a href='./image_upload.php?id=$id'>" . $plan . "</a><br><br>";
}
?>
</div></div>
<footer>
				<h1 id='ebato_inn'>Ebato.Inn</h1>
			</footer>

</body>
</html>
