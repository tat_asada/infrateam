<?php
session_start ();

if (! isset ( $_SESSION ['login_flg'] )) {
	header ( "Location: login.php" );
}

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|メニュー</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
	<header align="center">
		<h1>管理画面</h1>
	</header>
	<div id="main">
		<div id="side">
			<ul>
				<li>
					<button id='top'>
						<a href='./admin_index.php'>トップ</a>
					</button>
				</li>
				<li>
					<button id='plan'>
						<a href="./plan_info.php">プラン情報</a>
					</button>
				</li>
				<li>
					<button id='reserve'>
						<a href="./reserve.php">予約情報</a>
					</button>
				</li>
				<li>
					<button id='img'>
						<a href='./image_update.php'>画像変更</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./logout.php>ログアウト</a>
					</button>
				</li>
			</ul>
		</div>
		<div id="contents">

			<div class='center'>
				<h1>メニュー</h1>
				<br>
				<p>
					<font color='green'>左のメニューから閲覧・変更したい情報を選択してください</font>
				</p>
			</div></div></div>
			<footer align="center">
				<h1 id='ebato_inn'>Ebato.Inn</h1>
			</footer>

</body>
</html>
