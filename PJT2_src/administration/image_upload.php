<?php
session_start();
// セッションがなかったらログインページにリダイレクト
if (!isset($_SESSION['login_flg'])) {
	header( "Location: login.php" ) ;
}
// DBに接続
require ("../db_connect.php");

// image_updateからGETで飛んできたidを$idに格納
$id = $_GET['id'];

// planテーブルからidが$idのプランネームをセレクトして$planに格納
$stmt = $dbh->query("select plan_name from plan where id = $id");
$result =$stmt->fetch(PDO::FETCH_ASSOC);
$plan=$result['plan_name'];
?>
<!DOCTYPE html>
<htmle>
<head>
<title>管理画面|プラン画像・変更</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
<header align="center"><h1>管理画面</h1></header>
<div id="main">
<div id="side">
<ul>
				<li>
					<button id='top'>
						<a href='./admin_index.php'>トップ</a>
					</button>
				</li>
				<li>
					<button id='plan'>
						<a href='./plan_info.php'>プラン情報</a>
					</button>
				</li>
				<li>
					<button id='reserve'>
						<a href='./reserve.php'>予約情報</a>
					</button>
				</li>
				<li>
					<button id='img'>
						<a href='./image_update.php'>画像変更</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href='./logout.php'>ログアウト</a>
					</button>
				</li>
			</ul>
</div>
<div id="contents">
<h1>「<?php echo $plan ?>」の画像の変更</h1>
<br>
<p><font color='green'>画像をアップロードしてください</font></p>
<div id='image_upload1'>
<?php
// $jpgと$pngに保存されているファイルのパスを格納
$jpg = "../images/plan$id.jpg";
$png = "../images/plan$id.png";

// $jpgか$pngのファイルが存在している時、現在の画像を表示
// 存在しないときは"画像がありません"と表示
if (file_exists($jpg) or file_exists($png)) {
	if (file_exists($jpg)){
		echo "現在の画像<br><br>"."<image src=$jpg height = '180px'>";
	}
	if (file_exists($png)){
		echo "現在の画像<br><br>"."<image src=$png height = '180px'>";
	}
}else {

	echo "<font color='gray'>画像がありません</font>";

}
?>
</div>
<div id= 'image_upload2'>
<p>新しい画像をアップロードしてください</p>
<form method='post' enctype='multipart/form-data'>
<input type='file' name='image'>
<br><br>
<input type='submit' name='change' value='画像を変更' class='upload_btn'>
</div>
</form>
<?php
// 画像がアップロードされている時、画像を保存する
// $pathと$kakuchoでpathinfoを使ってファイルの拡張子を取得
// $uploadfileに保存先のパスとファイル名を格納
if ($_FILES) {
	$path = pathinfo ( $_FILES ['image'] ['name'] );
	$kakucho = $path ['extension'];

	$file_dir = "../images/";
	$uploadfile = "$file_dir" . "plan" . $id . "." . $kakucho;
	move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $uploadfile );
}

?>
</div>
</div>
</div>
<footer align="center"><h1 id='ebato_inn'>Ebato.Inn</h1></footer>
</body>
</html>
