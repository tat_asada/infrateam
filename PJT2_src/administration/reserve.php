<?php
session_start();

if (!isset($_SESSION['login_flg'])) {
	header( "Location: login.php" ) ;
}

require_once '../function.php';
require_once ("../db_connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|予約情報</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
<header align="center"><h1>管理画面</h1></header>
<div id="main">
<div id="side">
<ul>
<li>
<button id='top'><a href='./admin_index.php'>トップ</a></button>
</li>
<li>
<button id='plan'><a href="./plan_info.php">プラン情報</a></button>
</li>
<li>
<button id='reserve'><a href="./reserve.php">予約情報</a></button>
</li>
<li>
<button id = 'img'><a href='./image_update.php'>画像変更</a></button>
</li>
<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
<li>
<button id='logout'><a href=logout.php>ログアウト</a></button>
</li>
</ul>
</div>
<div id="contents">
<h1>予約情報</h1>
<br>
<form method='POST'>
<input type="date" name="date">
<input type='submit' name='display' value='予約一覧を表示' >
</form>
</div><br><br>
<?php
$date=get_post('date');
$stmt = $dbh->query("SELECT reserve_date FROM information where reserve_date = '$date'");
$result = $stmt->fetch(PDO::FETCH_ASSOC);
$reserve_date=$result['reserve_date'];

$stmt = $dbh->query("SELECT * FROM information where reserve_date = '$date'");

if ($reserve_date != ""){
	foreach ($stmt as $row){
		$id=$row['id'];
		$name=$row['name'];
		$phone=$row['phone_number'];
		$mail=$row['mail_address'];
		$room=$row['room'];
		$persons=$row['persons'];
		$reserve_date_new=$row['reserve_date'];
		$reception_date=$row['reception_date'];
		$plan=$row['plan_id'];

		//プランIDをプラン名に変換
		$stmt = $dbh->query("SELECT plan_name FROM plan WHERE id = '$plan'");
		$re=$stmt->fetch(PDO::FETCH_ASSOC);
		$plan_name=$re['plan_name'];


		echo "<table align= 'center'  id='reserve_info' >".
			 "<tr><th>お名前</th><td>".$name."</td></tr>".
			 "<tr><th>電話番号</th><td>".$phone."</td></tr>".
			 "<tr><th>メールアドレス</th><td>".$mail."</td></tr>".
			 "<tr><th>部屋数</th><td>".$room."</td></tr>".
			 "<tr><th>人数</th><td>".$persons."</td></tr>".
			 "<tr><th>予約日</th><td>".$reserve_date."</td></tr>".
			 "<tr><th>受付日</th><td>".$reception_date."</td></tr>".
			 "<tr><th>プラン</th><td>".$plan_name."</td></tr>".
		//	"<form method='GET' action='./reserve_update.php'>".
		//	 "<input type='hidden' name='id' value='$id'>".
			 "<a href='reserve_update.php?id=$id'><input type='submit' value='予約情報変更' button id='reserve_btn2'></a>";
			 "</form>"."</table>";
	}
}
if(!empty($date)){
	if($reserve_date == ""){
		echo "<font color='red'>".$date."に予約はありません</font>";
	}
}
?>

</div>
<footer align="center"><h1 id='ebato_inn'>Ebato.Inn</h1></footer>
</body>
</html>
