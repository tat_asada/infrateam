<!DOCTYPE html>
<html>
<head>
<title>管理画面|ログアウト</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
<header align="center"><h1><a href='./admin_index.php'>管理画面</a></h1></header>
<div id="main">
<div id="contents">
<div id="logoutpage">
<h1>ログアウト</h1>
<?php
session_start();
// ログインフラグがなければ、ログインページからログインしてくださいと表示し、リンクを下に表示
if(!isset($_SESSION['login_flg'])){
	echo "ログインページからログインしてください<br><br>".
	"<a href='login.php'>ログインページへ</a>";
}
// ログインフラグをもってきた場合、セッションを破棄してログアウトしたと表示し、ログインページへのリンクを表示
if (isset($_SESSION['login_flg'])){
	$_SESSION = array();
	session_destroy();
	echo "<br>ログアウトしました<br><br>";
	echo "<br><br><a href='login.php'>ログインページへ</a><br>";
}
?>
	</div>
</div>
</div>
<footer align="center"><h1 id='ebato_inn'>Ebato.Inn</h1></footer>
</body>


</html>
