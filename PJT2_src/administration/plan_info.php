<?php
session_start ();

if (! isset ( $_SESSION ['login_flg'] )) {
	header ( "Location: login.php" );
}

require_once ("../db_connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|プラン情報</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
	<header align="center">
		<h1>管理画面</h1>
	</header>
	<div id="main">
		<div id="side">
			<ul>
				<li>
					<button id='top'>
						<a href='./admin_index.php'>トップ</a>
					</button>
				</li>
				<li>
					<button id='plan'>
						<a href='./plan_info.php'>プラン情報</a>
					</button>
				</li>
				<li>
					<button id='reserve'>
						<a href='./reserve.php'>予約情報</a>
					</button>
				</li>
				<li>
					<button id='img'>
						<a href='./image_update.php'>画像変更</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href='./logout.php'>ログアウト</a>
					</button>
				</li>
			</ul>
		</div>
		<div id="contents">
		<div align="center" id='plan_info_div'>
			<h1>プラン情報</h1>
<?php
$stmt = $dbh->query ( "select * from plan" );

foreach ( $stmt as $row ) {
	$plan = $row ['plan_name'];
	$id = $row ['id'];
	$detail = $row ['detail'];
	$price = $row ['plan_price'];
	$flg = $row ['delete_flg'];
	echo "<table border ='1' id='plan_info'>";
	echo "<tr><th>プラン名</th>" . "<td class='td1'>" . $plan . "</td><td rowspan='2'>".
			"<form action='./plan_update.php'>" .
			"<input type='hidden' name='id' value='$id'>" .
			"<input type=submit value='変更' font-color='white'>"."</td></tr>" .
			"</form>".
			"<tr><th>価格</th>" . "<td>" . $price . "円/人" . "</td>" .

		 "<br><br>".
		 "</table>";
}

?>
			</div>
		</div>
		<footer align="center">
			<h1 id='ebato_inn'>Ebato.Inn</h1>
		</footer>

</body>


</html>
