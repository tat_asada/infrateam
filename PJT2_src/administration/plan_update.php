<?php
session_start ();

if (! isset ( $_SESSION ['login_flg'] )) {
	header ( "Location: login.php" );
}

require_once '../function.php';
require ("../db_connect.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>管理画面|プラン情報</title>
<link rel="stylesheet" style="text/css" href="./admin.css">
</head>

<body>
	<header align="center">
		<h1>管理画面</h1>
	</header>
	<div id="main">
		<div id="side">
			<ul>
				<li>
					<button id='top'>
						<a href='./admin_index.php'>トップ</a>
					</button>
				</li>
				<li>
					<button id='plan'>
						<a href="./plan_info.php">プラン情報</a>
					</button>
				</li>
				<li>
					<button id='reserve'>
						<a href="./reserve.php">予約情報</a>
					</button>
				</li>
				<li>
					<button id='img'>
						<a href='./image_update.php'>画像変更</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=./plan_add.php>プラン追加</a>
					</button>
				</li>
				<li>
					<button id='logout'>
						<a href=logout.php>ログアウト</a>
					</button>
				</li>
			</ul>
		</div>
		<div id="contents">
			<h1>プラン情報変更</h1>

<?php
// var_dump($_POST);
$id = $_GET ['id'];

$stmt = $dbh->query ( "select * from plan where id = '$id'" );

$result = $stmt->fetch ( PDO::FETCH_ASSOC );
$delete = $result ['delete_flg'];
$plan = $result ['plan_name'];
$detail = $result ['detail'];
$price = $result ['plan_price'];

if ($delete == 0) {
	$display = "表示";
}
if ($delete == 1) {
	$display = "非表示";
}
?>
<form method="post" id ='form'>
				<table align='center' id='plan_info'>

					<tr>
						<th>プラン名<font color='#00008B'>（変更前）</th>
						</font>
						<td><?php echo $plan ?></td>
					</tr>

					<tr>
						<th>プラン名<font color='#DC143C'>（変更後）</th>
						<td><input type='text' name='plan' placeholder='新しいプラン名'></td>
						</font>
					</tr>

					<tr>
						<th>プラン概要<font color='#00008B'>（変更前）</th>
						</font>
						<td><?php echo $detail ?></td>
					</tr>

					<tr>
						<th>プラン概要<font color='#DC143C'>（変更後）</th>
						</font>
						<td><textarea rows="10" cols="40" name='detail'
								placeholder='次の行にいくときは改行してください'></textarea></td>
					</tr>

					<tr>
						<th>プラン金額<font color='#00008B'>（変更前）</th>
						</font>
						<td><?php echo $price ?></td>
					</tr>
					<tr>
						<th>プラン金額<font color='#DC143C'>（変更後）</font>

						<td><input type='number' name='price'></td>
					</tr>
					<tr>
						<th>現在の設定：<?php echo $display ?></th>
						<td><input type='radio' name='express' value=0>表示する <br>
						<input type='radio' name='express' value=1>表示しない</td>
					</tr>

					<input type='hidden' name='id' value=$id>
					<input type='submit' name='change' value='変更' id='change'>
					</form>
				</table>

		</div>
		<footer> </footer>

</body>


</html>

<?php
$update_plan = get_post ( 'plan' );
$update_detail = get_post ( 'detail' );
$update_price = get_post ( 'price' );
$update_express = $_POST ['express'];
$change = get_post ( 'change' );

require ("../db_connect.php");
if ($change == '変更') {
	if (! empty ( $update_plan )) {
		$sql = $dbh->query ( "update plan set plan_name='$update_plan' where id='$id'" );
		$stmt->execute ( $sql );
	}
	if (! empty ( $update_detail )) {
		$sql = $dbh->query ( "update plan set detail='$update_detail' where id='$id'" );
		$stmt->execute ( $sql );
	}
	if (! empty ( $update_price )) {
		$sql = $dbh->query("update plan set plan_price='$update_price' where id='$id'");
		$stmt->execute($sql);
	}
	if ( $update_express !== "") {
		$sql = $dbh->query("update plan set delete_flg='$update_express' where id='$id'");
		$stmt->execute($sql);
	}
	header( "Location: ./plan_update.php?id=$id" );
	exit();
}
?>
<footer align="center">
	<h1 id='ebato_inn'>Ebato.Inn</h1>
</footer>
