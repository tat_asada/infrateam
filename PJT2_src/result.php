<html>
<head>
<link rel="stylesheet" style type="text/css" href="css/common.css">
</head>
<body>
<?php

########--セッション--#####################################

session_start();

require("db_connect.php");
require("function.php");

if (empty($_SESSION['resist'])){
	header('Location: ./form.php');
}
//htmlヘッダー部分の読込み
require_once ("./main_down.php");

$reserve_day=$_POST['reserve_date'];
$stmt = $dbh->query("select * from information where reserve_date = '$reserve_day'");
$nokori_room = $stmt->rowCount();

$stmt = $dbh->query("select * from information where id = 1");
foreach ($stmt as $row) {
	$maxcount=$row['maxcount'];
}

if (!empty($_POST)) {

	################--配列の準備--########################################
	$insert_data = array(

	"name" => $_POST['name'],
	"phone_number" => $_POST['phone_number'],
	"mail_address" => $_POST['mail_address'],
	"room" => $_POST['room'],
	"persons" => $_POST['persons'],
	"total_price" => $_POST['total_price'],
	"reserve_date" => $_POST['reserve_date'],
	"reception_date" => $_POST['time'],
	"plan_id" => $_POST['plan_id']
	);
	################--データの準備--#######################################

	$stmt = $dbh->prepare("
				INSERT INTO information
				(name,
				phone_number,
				mail_address,
				room,
				persons,
				total_price,
				reserve_date,
				reception_date,
				plan_id)
				VALUES (?,?,?,?,?,?,?,?,?)
				");

	$stmt->bindValue(1, $insert_data["name"]);
	$stmt->bindValue(2, $insert_data["phone_number"]);
	$stmt->bindValue(3, $insert_data["mail_address"]);
	$stmt->bindValue(4, $insert_data["room"]);
	$stmt->bindValue(5, $insert_data["persons"]);
	$stmt->bindValue(6, $insert_data["total_price"]);
	$stmt->bindValue(7, $insert_data["reserve_date"]);
	$stmt->bindValue(8, $insert_data["reception_date"]);
	$stmt->bindValue(9, $insert_data["plan_id"]);

	///////////////トランザクションはじめ
	$dbh->beginTransaction();

////////////////////空き部屋確認
	if ($nokori_room < $maxcount){
	//////////////実行
		if ($stmt->execute()) {


		/////////////////予約成功の場合
		########--新しく登録されたIDを取ってくる--############
		$id =$dbh->lastInsertId();

		echo "<br><h1 align=\"center\">ご予約が完了しました。</h1><br>
				<p align=\"center\">以下の内容で承りましたのでご変更があればご連絡ください。</p><br><br>";

		echo "<table align=\"center\">";
		////////////////個人情報テーブルからIDで検索
		$stmt = $dbh->query("SELECT * FROM information where id = ".$id.";");
		################----###################################
		foreach ($stmt as $row) {
			echo"<caption>受付日".$row['reception_date']."</caption>".
					"<tr><td><th>代表者</th></td><td>".$row['name']."</td><th>様</th></tr>".
					"<tr><td><th>お電話番号</th></td><td>".$row['phone_number']."</td></tr>".
					"<tr><td><th>メールアドレス</th></td><td>".$row['mail_address']."</td></tr>".
					"<tr><td><th>ご予約日</th></td><td>".$row['reserve_date']."</td></tr>".
					"<tr><td><th>ご利用部屋数</th></td><td>".$row['room']."</td><th>部屋</th></tr>".
					"<tr><td><th>ご利用人数</th></td><td>".$row['persons']."</td><th>名様</th></tr>".
					"<tr><td><th>合計金額</th></td><td>".$row['total_price']."</td><th>円</th></tr>";
			$_SESSION['resist'] = "";
		}
		echo "</table>";
		$dbh->commit();

	}else{

		echo "登録に失敗しました
			 		もう一度ご登録ください
			 		<form action='form.php'><button>戻る</button></form>";
		$_SESSION['resist'] = "";
	}
	}elseif($nokori_room >= $maxcount){


		$dbh->rollBack();

		echo "<p>";
		echo "申し訳ありません。".$reserve_day."は部屋数に空きがありません。"."<br>"
				."お手数ですが、もう一度別の日付を入力した上でご登録お願いいたします。";
		echo "<table><tr>";
		echo "<td><form  method='POST' action='form.php'>";
		echo "<input type='hidden' name='name' value='$name'>";
		echo "<input type='hidden' name='phone_number' value='$phone'>";
		echo "<input type='hidden' name='mail_address' value='$mail'>";
		echo "<input type='hidden' name='room' value='$room'>";
 		echo "<input type='hidden' name='plan_id' value='$plan_id'>";
		echo "<input type='hidden' name='total_price' value='$totalprice'>";
		echo "<input type='hidden' name='reserve_date' value='$reserve'>";
		echo "<input type='hidden' name='time' value='$time'>";
		echo "<input type='submit' value='戻る'>";
		echo "</td></tr></form></table>";

				echo "</p>";
				$_SESSION['resist'] = "";;

	}


}







?>
<footer></footer>
</body>
</html>