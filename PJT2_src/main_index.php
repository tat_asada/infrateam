<!DOCTYPE html>
<head>
<link rel="stylesheet" style type="text/css" href="css/common.css">
<title>旅の宿 江波戸</title>
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="./images/favicon3.ico" >
</head>
<body>
	<div id="headWrap">
		<div id="header">
			<h1>
				<a href="./index.php">旅の宿 江波戸</a>
			</h1>
			<div id="pr">
				<p>極上のひと時を思い出に刻む</p>
			</div>
			<div id="gnavi">
				<ul>
					<li>お電話からのご予約は<br /> <span class="tel">0120-000-000</span></li>
				</ul>
			</div>
		</div>
		<!-- /#header -->
	</div>
	<!-- /#headWrap -->
	<div id="menu">
		<ul>
			<li class="home"><a href="./index.php">ホーム</a></li>
			<li><a href="./room.php">お部屋</a></li>
			<li><a href="./system.php">サービス・設備</a></li>
			<li><a href="./food.php">お料理</a></li>
			<li><a href="./plan.php">プラン一覧</a></li>
			<li><a href="./access.php">アクセス</a></li>
			<li><a href="./form.php" target="_blank">予約</a></li>
		</ul><div class="clear"></div>
	</div>
	<!-- /#menu -->