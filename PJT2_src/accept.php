<html>
<head>
<link rel="stylesheet" style type="text/css" href="css/common.css">
</head>
<body>
<!--<div id = "accept">-->
<?php

####--セッション--####
session_start();
$_SESSION['resist'] = "entry";

require("db_connect.php");
require("function.php");

//htmlヘッダー部分の読込み
require_once ("./main_down.php");

$stmt = $dbh->query("select * from information where id = 1");
foreach ($stmt as $row) {
	$maxcount=$row['maxcount'];
}

####--エラーの関数--####
function get_data_errors($array){
	//初期値
	$message = "";
	// 名前
	if(empty($array["name"])) {
		$message .= "お名前を入力してください<br>\n";
	}
	//電話番号
	if(empty($array["phone_number"])) {
		$message .= "お電話番号を入力してください<br>\n";
	}
	elseif(!is_numeric($array["phone_number"])){
		$message .= "お電話番号は半角数字
			( - ﾊｲﾌﾝなし)でご入力ください<br>\n";
	}
	//メールアドレス
	if(empty($array["mail_address"])) {
		$message .= "メールアドレスが入力されていません<br>\n";
	}

	if (preg_match("/@/" , $array["mail_address"]) == 0){
		$message .= "メールアドレスに@が入っておりません。
			お手数ですがもう一度ご入力してください。<br>\n";
	}
	if(empty($_POST["agree"])) {
		$message .= "規約に同意していません<br>\n";
	}

	####--人数以上の部屋数の予約--####

	///////room
	$room = get_post("room");
	///////人数を格納
	$per = get_post("persons");

	if ( $per < $room ){
		$message .= "ご予約の部屋数が多すぎます<br>\n";
	}
	####--キャパシティ以上の人数の予約--####
	if ( ($room * 4) <= $per  ){
		$message .= "1部屋4名様までとなっております<br>\n";
	}

	####--過去の日付への予約--####

	///////予約時間
	$reserve = get_post("reserve_date");
	///////受付の日付を格納
	$time = date('Y-m-d');


	if ( strtotime($time) > strtotime($reserve)){
		$message .= "過去へのご予約は承れません<br>\n";
	}

	return $message;
	//echo "<form  action='./form.php'><button >変更</button></form>";
}
####--メッセージ表示準備--####
$success_message = "";
$error_message = "";

####--ポストデータのあるとき####
if (!empty($_POST)) {


			###--POSTを変数に格納--####

	///////名前
	$name = get_post("name");
	///////電話番号
	$phone = get_post("phone_number");
	///////mail
	$mail = get_post("mail_address");
	///////room
	$room = get_post("room");
	///////人数を格納
	$per = get_post("persons");
	///////プランidを格納
	$plan_id = get_post("plan_id");
	///////DBからプランの金額を探す
	$plan = $dbh->query("SELECT plan_price from plan where id =$plan_id;");
	$row = $plan->fetch();
	///////プランの金額を格納
	$totalprice = $row['plan_price'];
	///////合計金額の計算と格納
	$totalprice = $totalprice * $per;
	///////予約時間
	$reserve = get_post("reserve_date");
	///////受付の日付を格納
	$time = date('Y-m-d');

	}

	###--予約部屋数の確認--###


$stmt = $dbh->query("select * from information where reserve_date = '$reserve'");
$room1=0;
$exist_room=0;
foreach ($stmt as $row){
	$room1=$row['room'];
	$exist_room=$exist_room+$room1;
}
##############################################################
//現在部屋数がmaxより下で現在部屋数+予約部屋数がmax以下なら予約
if ($exist_room < $maxcount and $exist_room + $room <= $maxcount){

	$insert_data = array(

			"name" => $name,
			"phone_number" => $phone,
			"mail_address" => $mail,
			"room" => $room,
			"persons" => $per,
			"totalprice" => $totalprice,
			"reserve_date" => $reserve,
			"reception_date" => $time
	);

	echo "<div id ='error'>";
	// エラーチェック
	$errors = get_data_errors($insert_data);
	if ($errors) {
		// エラーがあった場合
		$error_message = $errors;
		echo "<hr>";
		echo $errors;
		echo "<hr>";
		echo "<form action='./form.php' method='POST'>";
		echo "<input type='hidden' name='name' value='$name'>";
		echo "<input type='hidden' name='phone_number' value='$phone'>";
		echo "<input type='hidden' name='mail_address' value='$mail'>";
		echo "<input type='hidden' name='room' value='$room'>";
		echo "<input type='hidden' name='plan_id' value='$plan_id'>";
		echo "<input type='hidden' name='total_price' value='$totalprice'>";
		echo "<input type='hidden' name='reserve_date' value='$reserve'>";
		echo "<input type='hidden' name='time' value='$time'>";
		echo "<input type='submit' value='戻る'>";
		echo "</form>";
	}else{
		echo "</div>";

		##--//予約内容の表示//--###
		echo "<div align='center' id=accept>";
		echo"<h1>この内容で予約します。
		よろしいですか？<h1>";

		//プラン表示
		echo "<h2>ご予約プラン</h2>";
		echo "<table border ='1' id='formtable'>";
		echo "<tr>"."<th>御予約日時</th>"."<td>".$reserve."</tr></td>".
				"<tr>"."<th>御予約人数</th>"."<td>".$per."名様</tr></td>".
				"<tr>"."<th>御利用部屋数</th>"."<td>".$room."部屋</tr></td>";
		echo "</table>";

		//個人情報
		echo	" <h2>お客様情報</h2>";
		echo "<table border ='1' id='formtable'>";
		echo "<tr>"."<th>お名前</th>"."<td>".$name."様</th></td></tr>".
				"<tr>"."<th>電話番号</th>"."<td>".$phone."</th></td></tr>".
				"<tr>"."<th>メールアドレス</th>"."<td>".$mail."</td></tr>";

		//最終確認ボタン
		echo "</table><tr><td><form  method='POST' action='form.php'>";
		echo "<input type='hidden' name='name' value='$name'>";
		echo "<input type='hidden' name='phone_number' value='$phone'>";
		echo "<input type='hidden' name='mail_address' value='$mail'>";
		echo "<input type='hidden' name='room' value='$room'>";
		echo "<input type='hidden' name='plan_id' value='$plan_id'>";
		echo "<input type='hidden' name='total_price' value='$totalprice'>";
		echo "<input type='hidden' name='reserve_date' value='$reserve'>";
		echo "<input type='hidden' name='time' value='$time'>";
		echo "<input type='submit' value='変更'>";
		echo "</form></td>";

		echo "<td><form  method='POST' action='result.php'>";
		echo "<input type='hidden' name='name' value='$name'>";
		echo "<input type='hidden' name='phone_number' value='$phone'>";
		echo "<input type='hidden' name='mail_address' value='$mail'>";
		echo "<input type='hidden' name='room' value='$room'>";
		echo "<input type='hidden' name='plan_id' value='$plan_id'>";
		echo "<input type='hidden' name='persons' value='$per'>";
		echo "<input type='hidden' name='total_price' value='$totalprice'>";
		echo "<input type='hidden' name='reserve_date' value='$reserve'>";
		echo "<input type='hidden' name='time' value='$time'>";
		echo "<input type='submit' name='ok' value='予約する'>";
		echo "</td></tr></form></table>";
		echo "</div>";
	}
}
//部屋数に空きが無い場合の表示画面
echo "<div align='center' style='margin-top: 40px'>";
if($exist_room >= $maxcount){
	echo "申し訳ありません。".$reserve."は部屋数に空きがございません。"."<br>"
			."お手数ですが、もう一度別の日付を入力した上でご登録お願いいたします。";
			require("backbutton.php");
}elseif($exist_room+$room > $maxcount){
	echo "申し訳ありません。".$reserve."は予約が".$exist_room
	."件入っておりますので残り予約可能部屋数は";
	echo $maxcount-$exist_room."部屋になります。"."<br>"
			."お手数ですが、予約部屋数を変更した上でもう一度ご登録お願いいたします。";
			require("backbutton.php");
}
echo "</div>";
?>
</div>
<footer></footer>
</body>
</html>