<?php
require_once ("./main_index.php");
?>
<div>
	<ol class="topic-path">
	<li class="first"><a href="./index.php">ホーム</a></li>
	<li>お料理</li>
	</ol>
</div>
<div id="main">
	<div class="page_header">
		<img src="./images/food2.jpg"><h2>お料理</h2>
	</div>
	<div class="jump">
	<ul>
	<li><a href="#main_morning"><img src="./images/morning_jump.jpg"><br>朝食</a></li>
	<li><a href="#main_dinner"><img src="./images/dinner_jump.jpg"><br>夕食</a></li>
	</ul>
	</div>
	<div id="main_morning">

		<p>朝食</p>
		<hr width="830px">
		<p><font size=3>　四季折々の地元野菜や旬の食材を調達する等、素材選びはもちろんのこと、<br>
		調理方法にもこだわりあり。朝に調理場で作られる味の濃い豆腐、朴葉みそ、<br>
		鮮度が勝負で販売はできない「岩魚の一夜干し」。いずれも「旅の宿江波戸」でしか食べられない味です。</font></p>
		<img src="./images/morning.jpg" class="img1"> <img
			src="./images/morning2.jpg" class="img1"> <img
			src="./images/morning3.jpg" class="img1">
		<br><br>
		<img src="./images/morning5.jpg" class="img2"> <img
			src="./images/morning6.jpg" class="img2">
	</div>
	<div id="main_dinner">
		<p>夕食</p>
		<hr width="830px">
		<p><font size="3">調理長『厳選』の旬の幸を舌で味わい目で楽しみ、お食事はもちろん個室でゆっくりと。
		<br>上質を当たり前に行う。それが旅の宿江波戸の誇りです。
		<br>旬の野菜と共に産地直送の海の幸も組み合わせ、季節と技にこだわりをもってご用意致します。</font></p>
		<img src="./images/dinner.jpg" class="img2"> <img
			src="./images/dinner2.jpg" class="img2">
		<br><br>
		<img
			src="./images/dinner3.jpg" class="img1"> <img
			src="./images/dinner4.jpg" class="img1"> <img
			src="./images/dinner5.jpg" class="img1">
	</div>
</div><!-- /#main -->

<?php
require_once ("./main_down.php");

